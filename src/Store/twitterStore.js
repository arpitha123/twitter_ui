import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import rootReducer from '../Reducer/rootReducer';
import rootSaga from '../Saga/rootSaga';
import createSagaMiddleware from 'redux-saga';

const sagaMiddleware = createSagaMiddleware();

const twitterStore = createStore(rootReducer, applyMiddleware(logger, sagaMiddleware));

sagaMiddleware.run(rootSaga);

export default twitterStore;