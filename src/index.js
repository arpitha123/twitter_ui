import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.css'
import * as serviceWorker from './serviceWorker';
import twitterStore from './Store/twitterStore';
import {Provider} from 'react-redux'

ReactDOM.render(
  <React.StrictMode>
    <Provider store={twitterStore}>
    <App />
    </Provider>
    
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
