import {combineReducers} from 'redux';
import {reducer} from 'redux-form'
import twitterReducer from './twitterReducer'
const rootReducer=combineReducers({twitterReducer,form:reducer});
export default rootReducer;