import React    from "react";
import template from "./Tweet.jsx";

class Tweet extends React.Component {
  constructor(){
    super();
  }
  render() {
    return template.call(this);
  }

  getTime(timeInMs){
      return new Date(Number(timeInMs)).toDateString();
  }
}

export default Tweet;
