import "./Tweet.css";
import React from "react";

function template() {
  const { text, user, timestamp_ms, quoted_status, retweeted_status } = this.props.tweetInfo;
  return (
    <div className="tweet form-group">
      <div className="row form-group">
        <div className="col-sm-2">
          <img className='rounded-circle' width="50" height="50" src={user.profile_image_url} />
        </div>
        <div className="col-sm-10 text-left">
          <b>{user.name}</b> <span className="twitter-acc">@{user.screen_name}</span><span className="tweet-time">{this.getTime(timestamp_ms)}</span>
          <p>
            {quoted_status && quoted_status.text}
            {retweeted_status && retweeted_status.text}
          </p>
          <div className="text-primary">
            {text}
          </div>
        </div>
      </div>
      <div className="row form-group p-10">
        {
          quoted_status && quoted_status.entities && quoted_status.entities.media && quoted_status.entities.media.map((mediaObj) => {
            if (mediaObj.type = "photo") {
              return <img width="200px" className='Thumbnail' height="200px" src={mediaObj.media_url} />
            }
            if (mediaObj.type = "video") {
              return <video width="200px" height="200px" src={mediaObj.media_url} />
            }
          })

        }
      </div>

      {
        retweeted_status && <div className="row form-group p-10 retweet">
          <div className="col-sm-2">
            <img className='rounded-circle' width="50" height="50" src={retweeted_status.user.profile_image_url} />
          </div>
          <div className="col-sm-10 text-left">
            <b>{retweeted_status.user.name}</b> <span className="twitter-acc">@{retweeted_status.user.screen_name}</span>
            description
            <div className="text-primary">
            {retweeted_status.user.description}
          </div>
          </div>
        </div>

      }

    </div>
  );
};

export default template;
