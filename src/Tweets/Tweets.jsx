import "./Tweets.css";
import React from "react";
import Tweet from '../Tweet/index';
function template() {
  return (
    <div className="tweets">
       <div className="offset-sm-3 col-sm-6">
        {
          this.props.tweets.slice(0,this.state.defaultCnt).map((obj)=>{
              return <Tweet tweetInfo={obj} />
          })
        }
       {this.props.tweets.length > 10 && this.state.isShowMore && <a className='show-more' onClick={this.fnShowMore.bind(this)}>show More</a>}
       </div>
    </div>
  );
};

export default template;
