import React    from "react";
import template from "./Tweets.jsx";
import {connect} from 'react-redux'
class Tweets extends React.Component {
  constructor(){
    super();
    this.state={
      defaultCnt:10,
      isShowMore:true
    }
  }
  render() {
    return template.call(this);
  }
  fnShowMore(){
      this.setState({
        defaultCnt:this.props.tweets.length,
        isShowMore:false
      })
  }
}


const mapStateToProps=(state)=>{
  return {
    tweets:state.twitterReducer.searchData
  }
}

export default connect(mapStateToProps)(Tweets);
