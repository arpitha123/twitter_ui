import {takeLatest,put,call} from 'redux-saga/effects';
import ServerCall from '../Services/ServerCall';

function* fnGetTweets(){
   const res= yield call(ServerCall.fnGetReq,'tweets/get-data');
   yield put({
    type: 'TWITTER_DATA',
    payload: res.data
  })


}
function* twitterSaga(){
    yield takeLatest("GET-TWEETS",fnGetTweets)
}
export default twitterSaga;