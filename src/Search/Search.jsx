import "./Search.css";
import React from "react";

function template() {
  return (
    <div className="search container-fluid">
      <div className="row form-group">
        <div className="offset-sm-4 col-sm-3">
          <input type="text" class="form-control" placeholder="Search Twitter" ref="searchTxtRef" />
        </div>
        <div className="col-sm-5 text-left">
          <button class="btn btn-primary" onClick={this.fnSubmit.bind(this)}>submit</button>
        </div>
      </div>
      {
        this.state.searchData.length == 0 && <p className='text-dark text-center'>
           <b>No search results found</b>
        </p>
      }

    </div>
  );
};

export default template;
