import React from "react";
import template from "./Search.jsx";
import { connect } from 'react-redux'
class Search extends React.Component {

  constructor() {
    super();
    this.state = {
      searchData: []
    }
  }

  fnSubmit() {
    let searchText = this.refs.searchTxtRef.value.toLowerCase();
    let searchData = this.props.tweets.filter((obj) => {
      if (obj.text.toLowerCase().includes(searchText)) {
        return true;
      } else if (obj.quoted_status && obj.quoted_status.text.toLowerCase().includes(searchText)) {
        return true;
      } else if (obj.retweeted_status && obj.retweeted_status.text.includes(searchText)) {
        return true;
      }
    })
   
      this.setState({
        searchData: searchData
      })

    this.props.dispatch({
      type: 'SEARCH_DATA',
      payload: searchData
    })
  }
  render() {
    return template.call(this);
  }
}
const mapStateToProps = (state) => {
  return {
    tweets: state.twitterReducer.twitterData
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    dispatch
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Search);
