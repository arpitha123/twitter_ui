import React, { useEffect } from 'react';
import './App.css';
import store from './Store/twitterStore';
import Search from './Search/index';
import Tweets from './Tweets/index';
import Header from './Header/index';
import Footer from './Footer/index';
function App() {
  useEffect(() => {
    store.dispatch({
      type:'GET-TWEETS'
    })
    
  }, []);
  return (
    <div className="App">
      <Header />
      <Search />
      <Tweets />
      <Footer />
    </div>
  );
}

export default App;
